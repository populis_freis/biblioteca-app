package model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;


/**
 * Busca livros em um arquivo TXT no classpath do aplicativo
 * 
 * @author felipereis
 *
 */
public class AcervoArquivo implements Acervo {
	private final List<Livro> livros;

	public AcervoArquivo(String caminhoArquivo) throws FileNotFoundException, IOException {
		this.livros = leLivrosDoArquivo(caminhoArquivo);
	}

	/* idealmente não estaria publico */
	public List<Livro> leLivrosDoArquivo(String caminhoArquivo) throws FileNotFoundException, IOException {
		List<Livro> livros = new ArrayList<>();

		LineIterator iterator = IOUtils.lineIterator(this.getClass().getResourceAsStream(caminhoArquivo), "UTF-8");

		String linha;

		while (iterator.hasNext()) {
			linha = iterator.nextLine();
			
			livros.add(leLivro(linha));
		}

		return livros;
	}

	/* idealmente não estaria publico */
	public Livro leLivro(String linha) {
		Livro livro = new Livro();

		String[] colunas = linha.split(";");

		livro.setId(Integer.valueOf(colunas[0]));
		livro.setTitulo(colunas[1]);
		livro.setAutor(colunas[2]);
		livro.setQuantidadeDePaginas(Integer.valueOf(colunas[3]));
		livro.setDisponivel(Boolean.valueOf(colunas[4]));

		return livro;
	}

	@Override
	public List<Livro> getTodos() {
		return livros;
	}

	@Override
	public List<Livro> getDisponiveis() {
		List<Livro> disponiveis = new ArrayList<>();

		for (Livro l : getTodos()) {
			if (l.getDisponivel()) {
				disponiveis.add(l);
			}
		}

		return disponiveis;
	}

	@Override
	public List<Livro> getRetirados() {
		List<Livro> retirados = new ArrayList<>();

		for (Livro l : getTodos()) {
			if (!l.getDisponivel()) {
				retirados.add(l);
			}
		}

		return retirados;
	}

	@Override
	public List<Livro> getDisponiveis(Date dataVerificacao) {
		// TODO Implementar
		return null;
	}
}