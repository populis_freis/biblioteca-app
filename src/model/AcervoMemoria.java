package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Busca livros em uma lista guardada na memória
 * 
 * @author felipereis
 *
 */
public class AcervoMemoria implements Acervo {
	private final List<Livro> livros;

	public AcervoMemoria(List<Livro> livros) {
		this.livros = livros;
	}

	@Override
	public List<Livro> getTodos() {
		return livros;
	}

	@Override
	public List<Livro> getDisponiveis() {
		List<Livro> disponiveis = new ArrayList<>();

		for (Livro l : getTodos()) {
			if (l.getDisponivel()) {
				disponiveis.add(l);
			}
		}

		return disponiveis;
	}

	@Override
	public List<Livro> getRetirados() {
		List<Livro> retirados = new ArrayList<>();

		for (Livro l : getTodos()) {
			if (!l.getDisponivel()) {
				retirados.add(l);
			}
		}

		return retirados;
	}

	@Override
	public List<Livro> getDisponiveis(Date dataVerificacao) {
		// Implementar
		return null;
	}
}
