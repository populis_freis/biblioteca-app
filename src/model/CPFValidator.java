package model;

public class CPFValidator {


	public boolean valida(String cpf) {
		String cpfRegex = "^\\d{11}$";
		
		return cpf.matches(cpfRegex);
	}

}
