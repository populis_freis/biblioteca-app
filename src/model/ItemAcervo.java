package model;

public interface ItemAcervo {
	Integer getId();

	boolean getDisponivel();

	String getTitulo();
}
