package model;

import java.util.Date;
import java.util.List;

/**
 * 
 * TODO exercício: para que serve uma interface?
 * 
 * @author felipereis
 *
 */
public interface Acervo {

	/**
	 * Obtém todos os items do acervo
	 * 
	 * @return Lista de todos os itens do acervo
	 */
	public List<Livro> getTodos();

	/**
	 * Obtém apenas os itens que estão disponíveis para retirada.
	 * 
	 * @return lista de items disponíveis para retirada
	 */
	public List<Livro> getDisponiveis();

	/**
	 * Obtém apenas os itens que estão, estiveram ou estarão disponíveis para
	 * retirada na data de pesquisa
	 * 
	 * @param dataVerificacao
	 *            data a ser usada para verificaçõa de itens disponíveis
	 * @return lista de itens disponíveis para retirada naquela data
	 */
	public List<Livro> getDisponiveis(Date dataVerificacao);

	/**
	 * Obtém apenas os itens que estão retiradas no momento.
	 * 
	 * @return lista de itens que estão retiradaos.
	 */
	public List<Livro> getRetirados();

}
