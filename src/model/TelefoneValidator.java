package model;

public class TelefoneValidator {
	public boolean valida(String numeroTelefone) {
		String valida  = "^9?\\d{8}$";
		
		return numeroTelefone.matches(valida);
	}
}
