package model;

public class Livro implements ItemAcervo {
	private Integer id;
	private String titulo;
	private String autor;
	private Integer quantidadeDePaginas;
	private boolean disponivel;

	public Livro() {}

	public Livro(Integer id, String titulo, String autor, Integer quantidadeDePaginas, Boolean disponivel) {
		this.id = id;
		this.titulo = titulo;
		this.autor = autor;
		this.quantidadeDePaginas = quantidadeDePaginas;
		this.disponivel = disponivel;
	}
	
	@Override
	public String toString() {
		return getTitulo() + " :: " + getAutor();
	}

	@Override
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public Integer getQuantidadeDePaginas() {
		return quantidadeDePaginas;
	}

	public void setQuantidadeDePaginas(Integer quantidadeDePaginas) {
		this.quantidadeDePaginas = quantidadeDePaginas;
	}

	@Override
	public boolean getDisponivel() {
		return disponivel;
	}

	public void setDisponivel(boolean disponivel) {
		this.disponivel = disponivel;
	}
	
}
