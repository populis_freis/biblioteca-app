package test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import model.TelefoneValidator;

import org.junit.Before;
import org.junit.Test;


public class TelefoneValidatorTest {
	TelefoneValidator telefoneValidator;

	@Before
	public void iniciaValidator() {
		telefoneValidator = new TelefoneValidator();
	}
	
	@Test
	public void validaTelefoneValidoComNove() {
		assertTrue("Telefone Valido com nove", telefoneValidator.valida("980447727"));
	}
	@Test
	public void validaTelefoneValidoComOito() {
		assertTrue("Telefone Valido com oito", telefoneValidator.valida("30447727"));
	}	

	@Test
	public void invalidoTelefoneComLetraNove() {
		assertFalse("Telefone Invalido com letras nove", telefoneValidator.valida("980y47727"));
	}	
	@Test
	public void invalidoTelefoneComLetraOito() {
		assertFalse("Telefone Invalido com letras oito", telefoneValidator.valida("80y47727"));
	}
	@Test
	public void invalidoTelefoneComMaisdeNove() {
		assertFalse("Telefone Invalido com mais de nove", telefoneValidator.valida("9980047727"));
	}
	@Test
	public void invalidoTelefoneComMenosdeOito() {
		assertFalse("Telefone Invalido com menos de oito", telefoneValidator.valida("0047727"));
	}
	@Test
	public void invalidoTelefoneComecaLetra() {
		assertFalse("Telefone Invalido comeca letra", telefoneValidator.valida("a980447727"));
	}
	@Test
	public void invalidoTelefoneTerminaLetra() {
		assertFalse("Telefone Invalido termina letra", telefoneValidator.valida("980447727a"));
	}	
}