package test;

import java.util.ArrayList;
import java.util.List;

import model.Acervo;
import model.AcervoMemoria;
import model.Livro;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AcervoMemoriaTest {
	private Acervo acervoLivros;

	/**
	 * Inicia um acervo de livros com 3 volumes disponíveis e 2 retirados.
	 */
	@Before
	public void iniciaAcervo() {
		List<Livro> livros = new ArrayList<>();

		livros.add(new Livro(1, "Use a cabeça Jquery", "Ronan Cranley", 500, true));
		livros.add(new Livro(2, "Java Efetivo", "Joshua Block", 321, true));
		livros.add(new Livro(3, "Spring In Action", "Craig Walls", 424, true));
		livros.add(new Livro(4, "JSF e JPA", "Gilliard Cordeiro", 291, false));
		livros.add(new Livro(5, "JPA Eficaz", "Hébert Coelho", 173, false));
		
		acervoLivros = new AcervoMemoria(livros);
	}

	@Test
	public void obtemTodosOsVolumes() {
		Assert.assertEquals("Acervo possui 5 livros no total", 5, acervoLivros.getTodos().size());
	}

	@Test
	public void obtemVolumesRetirados() {
		Assert.assertEquals("Acervo possui 2 livros retirados", 2, acervoLivros.getRetirados().size());
	}

	@Test
	public void obtemVolumesDisponiveis() {
		Assert.assertEquals("Acervo possui 3 livros disponíveis", 3, acervoLivros.getDisponiveis().size());
	}

	@Test
	public void obtemTodosAposObterDisponiveis() {
		Assert.assertEquals("Acervo possui 5 livros no total", 5, acervoLivros.getTodos().size());
		Assert.assertEquals("Acervo possui 3 livros disponíveis", 3, acervoLivros.getDisponiveis().size());
		Assert.assertEquals("Acervo possui 5 livros no total após obter disponíveis", 5, acervoLivros.getTodos().size());
	}
}
