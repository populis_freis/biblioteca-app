package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import model.CPFValidator;

import org.junit.Before;
import org.junit.Test;

public class CPFValidatorTest {
	CPFValidator cpfValidator;

	@Before
	public void iniciaValidator() {
		cpfValidator = new CPFValidator();
	}

	@Test
	public void validaCPFValido() {
		assertTrue("CPF válido", cpfValidator.valida("32554073899"));
	}
	
	@Test
	public void invalidaCPFComCaracterNaoNumerico() {
		assertFalse("CPF com caractere não numérico é inválido", cpfValidator.valida("3255407389A"));
	}

	@Test
	public void invalidaCPFComMenosDe11Caracteres() {
		assertFalse("CPF com menos de 11 caracteres é inválido",
				cpfValidator.valida("11111"));
	}

	@Test
	public void invalidaCPFComMaisDe11Caracteres() {
		assertFalse("CPF com mais de 11 caracteres é inválido",
				cpfValidator.valida("111111111111111"));
	}
}