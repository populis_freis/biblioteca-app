package main;

import model.CPFValidator;

/**
 * Classe separada para validação
 * 
 * Princípios SOLID
 * 
 * S - Single responsibility principle a class should have only a single
 * responsibility (i.e. only one potential change in the software's
 * specification should be able to affect the specification of the class)
 *
 * 1- gerar jar pelo eclipse
 * 2- executar jar no cmd: java -jar nomejar.jar CPF
  * 
 * @author felipereis
 *
 */
public class AppCPF {
	public static void main(String[] args) {
		String cpf = args[0];

		CPFValidator cpfValidator = new CPFValidator();

		if (cpfValidator.valida(cpf)) {
			System.out.println("CPF válido");
		} else {
			System.out.println("CPF inválido");
		}
	}
}
