package main;

import java.io.FileNotFoundException;
import java.io.IOException;

import model.Acervo;
import model.AcervoArquivo;
import model.Livro;

public class AppAcervo {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Acervo acervo = new AcervoArquivo("repositorio.txt");
		
		for(Livro livro : acervo.getTodos()) {
			System.out.println(livro);
		}
		
	}
}
